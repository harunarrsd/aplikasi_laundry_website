<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @author Sony Surahman
*/
class Main extends CI_Controller {

	public function index()
	{
		$id = $this->session->userdata('id_user');
		if ($id=="") {
			$data['title'] = "Selamat Datang di Laundry Yo";
			$this->load->view('include',$data);
		}else{
			redirect('main/admin');
		}
	}
	public function sign_up()
	{
		$id = $this->session->userdata('id_user');
		if ($id=="") {
			$data['title'] = "Selamat Datang di Laundry Yo - Daftar Baru";
			$this->load->view('include',$data);
		}else{
			redirect('main/admin');
		}
	}
	public function sign_in()
	{
		$id = $this->session->userdata('id_user');
		if ($id=="") {
			$data['title'] = "Selamat Datang di Laundry Yo - Masuk";
			$this->load->view('pages/sign_in',$data);
		}else{
			redirect('main/admin');
		}
	}
	public function new_order()
	{
		$id = $this->session->userdata('id_user');
		if ($id!="") {
			$data = $this->model_laundry->getUserData();
			$data['title'] = "Laundry UKK - Pesanan Baru";
			$data['halaman'] = "Pesan Baru";
			$data['subhalaman'] = "Satuan atau Kiloan";
			$this->load->view('pages/new_order',$data);
		}else{
			redirect('main/sign_in');
		}
	}
	public function order(){
		$id = $this->session->userdata('id_user');
		if ($id!="") {
			$data = $this->model_laundry->getUser();
			$data['halaman'] = "Data Pesanan Pelanggan";
			$data['subhalaman'] = "";
			$data['title'] = ucfirst($data['nama']);
			$data['jnis'] = $this->model_laundry->getJenisCuci()->result();
			$data['allOrdersData'] = $this->model_laundry->getAllOrders();
			$this->load->view('pages/order',$data);
		}else{
			redirect('main/sign_in');	
		}
	}
	public function member(){
		$id = $this->session->userdata('id_user');
		if ($id!="") {
			
			$data = $this->model_laundry->getUser(); 
			$data['cOrder'] = $this->db->get_where('transaksi',array('status'=>'on_order'))->num_rows();
			$data['cTask'] = $this->db->get_where('transaksi',array('status'=>'on_task'))->num_rows();
			$data['cDone'] = $this->db->get_where('transaksi',array('status'=>'done'))->num_rows();
			$data['cCancel'] = $this->db->get_where('transaksi',array('status'=>'cancel'))->num_rows();
			$data['status'];
			if ($data['status']=="member") {

			}else{

			}
			$data['getmember'] = $this->db->get_where('user',array('type_user'=>'member'));
			
			$data['nama'];
			$data['halaman'] = "member";
			$data['subhalaman'] = "Control Panel";
			$data['title'] = ucfirst($data['nama'])." - Laundry Yo";
			
			$this->load->view('pages/member',$data);
		}else{
			redirect('main/sign_in');
		}
	}
	public function staff(){
		$id = $this->session->userdata('id_user');
		if ($id!="") {
			
			$data = $this->model_laundry->getUser(); 
			$data['cOrder'] = $this->db->get_where('transaksi',array('status'=>'on_order'))->num_rows();
			$data['cTask'] = $this->db->get_where('transaksi',array('status'=>'on_task'))->num_rows();
			$data['cDone'] = $this->db->get_where('transaksi',array('status'=>'done'))->num_rows();
			$data['cCancel'] = $this->db->get_where('transaksi',array('status'=>'cancel'))->num_rows();
			$data['status'];
			if ($data['status']=="member") {

			}else{

			}
			$data['getkaryawan'] = $this->db->get('karyawan');
			$data['nama'];
			$data['halaman'] = "Staff";
			$data['subhalaman'] = "Control Panel";
			$data['title'] = ucfirst($data['nama'])." - Laundry Yo";
			
			$this->load->view('pages/staff',$data);
		}else{
			redirect('main/sign_in');
		}
	}
	public function admin()
	{
		$id = $this->session->userdata('id_user');
		if ($id!="") {
			
			$data = $this->model_laundry->getUser(); 
			$data['cOrder'] = $this->db->get_where('transaksi',array('status'=>'on_order'))->num_rows();
			$data['cTask'] = $this->db->get_where('transaksi',array('status'=>'on_task'))->num_rows();
			$data['cDone'] = $this->db->get_where('transaksi',array('status'=>'done'))->num_rows();
			$data['cCancel'] = $this->db->get_where('transaksi',array('status'=>'cancel'))->num_rows();
			$data['status'];
			if ($data['status']=="member") {
				// Member
				$data['countmSuccess'] = $this->model_laundry->getSuccessOrder()->num_rows();
				$data['successOrder'] = $this->model_laundry->getSuccessOrder()->result();

				$data['countmCancel'] = $this->model_laundry->getCancelOrder()->num_rows();
				$data['cancelOrder'] = $this->model_laundry->getCancelOrder()->result();

				$data['countmTask'] = $this->model_laundry->getTaskOrder()->num_rows();
				$data['taskOrder'] = $this->model_laundry->getTaskOrder()->result();

				$data['countmOn'] = $this->model_laundry->getOnOrder()->num_rows();
				$data['onOrder'] = $this->model_laundry->getOnOrder()->result();

				// Member
				# code...
			}else{

			}
			$data['nama'];
			$data['halaman'] = "Dashboard";
			$data['subhalaman'] = "Control Panel";
			$data['title'] = ucfirst($data['nama'])." - Laundry Yo";
			
			$this->load->view('pages/main_admin',$data);
		}else{
			redirect('main/sign_in');
		}
	}
	public function profile()
	{
		$id = $this->session->userdata('id_user');
		if ($id!="") {
			$data = $this->model_laundry->getUser();
			$data['aktifitas'] = $this->model_laundry->getHistoryData();
			$data['nama'];
			$data['created_date'];
			$data['status'];
			$data['halaman'] = "User Profile";
			$data['subhalaman'] = "";
			$data['title'] = ucfirst($data['nama']);
			$this->load->view('pages/profile',$data);
		}else{
			redirect('main/sign_in');	
		}
	}
	public function editstaff(){
		$id = $this->session->userdata('id_user');
		if ($id!="") {
			
			$data = $this->model_laundry->getUser(); 
			$data['cOrder'] = $this->db->get_where('transaksi',array('status'=>'on_order'))->num_rows();
			$data['cTask'] = $this->db->get_where('transaksi',array('status'=>'on_task'))->num_rows();
			$data['cDone'] = $this->db->get_where('transaksi',array('status'=>'done'))->num_rows();
			$data['cCancel'] = $this->db->get_where('transaksi',array('status'=>'cancel'))->num_rows();
			$data['status'];
			if ($data['status']=="member") {

			}else{

			}
			$data['getkaryawan'] = $this->db->get('karyawan');
			$data['nama'];
			$data['halaman'] = "Staff";
			$data['subhalaman'] = "Control Panel";
			$data['title'] = ucfirst($data['nama'])." - Laundry Yo";
			
			$this->load->view('pages/editstaff',$data);
		}else{
			redirect('main/sign_in');
		}
	}
	public function acteditstaff(){
		$this->model_laundry->editstaff();
		redirect('main/staff');
	}
}