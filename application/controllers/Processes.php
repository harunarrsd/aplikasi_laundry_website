<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Processes extends CI_Controller {
	public function sign_up()
	{
		try {
			$n = $this->input->post('nama');
			$u = $this->input->post('username');
			$d = $this->input->post('date_created');
			$p = md5($this->input->post('password'));
			$this->model_laundry->p_sign_up($n,$u,$p,$d);
		} catch (Exception $e) {
			throw new Exception("Error Processing Request", 1);
		}
	}
	public function sign_in()
	{
		try {
			$u = $this->input->post('username');
			$p = md5($this->input->post('password'));
			$this->model_laundry->f_login($u,$p);
		} catch (Exception $e) {
			throw new Exception("Error Processing Request", 1);
		}
	}
	public function log_out()
	{
		$this->session->sess_destroy();
		redirect('main/');
	}
	public function get_harga_jeniscucian()
	{
		$id = $this->input->get('id_jenisC');
		$this->db->where('id_jenis',$id);
		$q = $this->db->get('t_jenis_cuci_kilo');
		$r = $q->num_rows();
		if ($r>0) {
			foreach ($q->result() as $djenis) {
				$data = array('nama'=>$djenis->nama,'harga'=>$djenis->harga);
			}
		}
		echo json_encode($data);
		// return $q;
	}
	public function act_order()
	{
		$id = $this->session->userdata('id_user');
		if ($id=="") {
			redirect('main/sign_in');
		}else{
			try {
				$this->model_laundry->f_act_order();
			} catch (Exception $e) {
				throw new Exception("Error Processing Request", 1);
			}
		}
		
	}
}