<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Model_laundry extends CI_Model
{
	function p_sign_up($n,$u,$p,$d)
	{
		if ($n=="" OR $u=="" OR $p=="") {
			echo "<script>alert('Data kosong!')</script>";
		}else{
			$data = array(
					'id_user'=>null,
					'username'=> $u,
					'password' => $p,
					'type_user' => 'member',
					'created_date' => $d,
					'status' => 'aktif'
				);
			$q = $this->db->insert('user',$data);
			$id = $this->db->insert_id();
			if ($q==TRUE) {
				$q = $this->db->query("INSERT INTO konsumen VALUES(null,'$id','$n','','','','','')");
				$this->db->query("INSERT INTO history VALUES(null,'$id','User Created','Kamu membuat akun di Laundry Yo!',NOW(),'aktif')");
				$this->f_login($u,$p);
				echo "<script>alert('sukses!')</script>";
			}else{
				echo "<script>alert('Gagal!')</script>";
			}
		}
		echo $n." ".$u." ".$p;
	}
	function f_login($u,$p)
	{
		$q = $this->db->query("SELECT * FROM user WHERE username='$u' AND password='$p'");
		$r = $q->num_rows();
		if ($r>0) {
			$d = $q->row();
			$session = array('id_user' => $d->id_user );
			$this->session->set_userdata($session);
			redirect('main/admin');
		}else{
			$this->session->set_flashdata('error','value');
			redirect('main/sign_in');
		}
	}

	// All Data USer
	function getUser()
	{
		$id = $this->session->userdata('id_user');
		$q = $this->db->query("SELECT * FROM user WHERE id_user='$id'")->row();
		if ($q->type_user=='admin') {
			$data = $this->getAdminData();
		}else if ($q->type_user=='karyawan') {
			$data = $this->getKaryawanData();
		}else if ($q->type_user=='member') {
			$data = $this->getUserData();
		}
		return $data;
	}
	function getUserData()
	{
		$id = $this->session->userdata('id_user');
		$q = $this->db->query("SELECT * FROM user u INNER JOIN konsumen k ON u.id_user=k.id_user WHERE u.id_user='$id'")->row();
		$data['nama'] = $q->nama_konsumen;
		$data['almt'] = $q->alamat_konsumen;
		$data['email'] = $q->email_konsumen;
		$data['created_date'] = $q->created_date;
		$data['status'] = $q->type_user;
		return $data;
	}
	function getKaryawanData()
	{
		$id = $this->session->userdata('id_user');
		$q = $this->db->query("SELECT * FROM user u INNER JOIN karyawan k ON u.id_user=k.id_user WHERE u.id_user='$id'")->row();
		$data['nama'] = $q->nama_karyawan;
		$data['almt'] = $q->alamat_karyawan;
		$data['status'] = $q->type_user;
		return $data;
	}
	function getAdminData()
	{
		$id = $this->session->userdata('id_user');
		$data['nama'] = "Administrator";
		$data['status'] = 'admin';
		return $data;
	}
	// Order Data
	function getAllOrder()
	{
		// $id = $this->session->userdata('id_user');
		// $q = $this->db->get_where('transaksi',array('id_konsumen'=>$id));
		// $r = $q->num_rows();
		// if ($r<1) {
		// 	echo "Kosong";
		// }else{			
		// 	$k = $q->row();
		// 	}else if ($k->status=='on_task') {
		// 	if ($k->status=='on_order') {
		// 		$data = $this->db->query("SELECT * FROM transaksi WHERE status='on_order' AND id_konsumen='$id'")->result();
		// 		$data = $this->db->query("SELECT * FROM transaksi WHERE status='on_task' AND id_konsumen='$id'")->result();
		// 	}else if ($k->status=='done') {
		// 	}else if ($k->status=='cancel') {
		// 		$data = $this->db->query("SELECT * FROM transaksi WHERE status='cancel' AND id_konsumen='$id'")->result();
		// 	}
		// 	return $data;
		// }
	}
	function getSuccessOrder()
	{
		$id = $this->session->userdata('id_user');
		$data = $this->db->query("SELECT * FROM transaksi WHERE status='done' AND id_konsumen='$id'");
		return $data;
	}
	function getCancelOrder()
	{
		$id = $this->session->userdata('id_user');
		$data = $this->db->query("SELECT * FROM transaksi WHERE status='cancel' AND id_konsumen='$id'");
		return $data;
	}
	function getTaskOrder()
	{
		$id = $this->session->userdata('id_user');
		$data = $this->db->query("SELECT * FROM transaksi WHERE status='on_task' AND id_konsumen='$id'");
		return $data;
	}
	function getOnOrder()
	{
		$id = $this->session->userdata('id_user');
		$data = $this->db->query("SELECT * FROM transaksi WHERE status='on_order' AND id_konsumen='$id'");
		return $data;
	}

	// Processes
	function f_act_order()
	{
		$id = $this->session->userdata('id_user');

		$jb = $this->input->post('jenis_barang');
		$jp = $this->input->post('jenis_paket');
				// $ta =$this->input->post('tgl_diambil');
				// $ts = $this->input->post('tgl_selesai');
		$b =$this->input->post('berat');
		$jc = $this->input->post('jenis_cuci');
		if ($jb=="" OR $jp=="" OR $b=="" OR $jc=="") {
			$msg = array('success'=>'false','msg'=>'form_kosong');
		}else{
			$qharga = $this->db->query("SELECT harga FROM t_jenis_cuci_kilo WHERE id_jenis='$jc'");
			foreach ($qharga->result_array() as $dharga) {
				$hrgnya = $dharga['harga'];
			};
			$q = $this->db->query("INSERT INTO transaksi VALUES(null,'$id','',NOW(),'','on_order')");
			if ($q==TRUE) {
				$idtrans = $this->db->insert_id();
				$sql = $this->db->query("INSERT INTO transaksi_detail VALUES(null,'$idtrans','$jb','$jc','$jp','','$b')");
				if ($jp=="express") {
					$hpaket = 5000;
				}else{
					$hpaket = 0;
				};

				$hrgasli = $b*$hrgnya;
				$subtotal = $hrgasli + $hpaket;
				$insertTarif = $this->db->query("INSERT INTO tarif VALUES(null,'$idtrans','','$hrgasli','$subtotal','')");
				$pesan = "Kamu telah memesan Pesanan Baru dengan detail berat barang : ".$b."Kg, Jenis Cuci : ".$jc.", Jenis Paket : ".$jp.", dan Jenis Barang : ".$jb."";
				$this->db->query("INSERT INTO history VALUES(null,'$id','new_order','$pesan',NOW(),'aktif')");
				$msg = array('success'=>'true');
			}else{
				$msg = array('success'=>'false');
			}
		}
		
		echo json_encode($msg);
	}
	function getHistoryData()
	{
		$id = $this->session->userdata('id_user');
		$q = $this->db->query("SELECT * FROM history WHERE id_konsumen='$id' ORDER BY tgl_history DESC")->result();
		return $q;
	}
	public function editstaff(){
		$nik = $this->input->post('nik');
		$nama_karyawan = $this->input->post('nama_karyawan');
		$alamat_karyawan = $this->input->post('alamat_karyawan');
		$telp_karyawan = $this->input->post('telp_karyawan');
		$jk_karyawan = $this->input->post('jk_karyawan');
		$status_karyawan = $this->input->post('status_karyawan');
		$uri = $this->input->post('id_karyawan');
		$sql = $this->db->query("UPDATE karyawan SET nik='$nik', nama_karyawan='$nama_karyawan', alamat_karyawan='$alamat_karyawan', telp_karyawan='$telp_karyawan', jk_karyawan='$jk_karyawan', status_karyawan='$status_karyawan' WHERE id_karyawan='$uri' ");
		return $sql;
	}
		function getAllOrders()
	{
		$q = $this->db->query("SELECT * FROM transaksi t INNER JOIN transaksi_detail td ON t.id_transaksi=td.id_transaksi INNER JOIN tarif tr ON tr.id_transaksi=t.id_transaksi INNER JOIN konsumen k ON k.id_user=t.id_konsumen LEFT JOIN t_jenis_cuci_kilo tj ON td.jenis_cuci=tj.id_jenis")->result();
		return $q;
	}
		function getJenisCuci()
	{
		return $jc = $this->db->get('t_jenis_cuci_kilo');
	}
		function getOrderBy($id)
	{
		if ($id=="") {
			redirect('main/order');
		}else{
			$q = $this->db->query("SELECT * FROM transaksi t INNER JOIN transaksi_detail td ON t.id_transaksi=td.id_transaksi INNER JOIN tarif tr ON tr.id_transaksi=t.id_transaksi INNER JOIN konsumen k ON k.id_user=t.id_konsumen LEFT JOIN t_jenis_cuci_kilo tj ON td.jenis_cuci=tj.id_jenis WHERE t.id_transaksi='$id'");
			if ($q->num_rows()<1) {
				redirect('main/order');
			}else{
				return $q->result();
			}
		}
	}
}