<div class="row">
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3>
					<?php 
						if ($status!='member') {
							echo $cOrder; 
						}else{
							echo $countmOn;
						}
					?>
				</h3>
				<p>On Orders</p>
			</div>
			<div class="icon">
				<i class="ion ion-bag"></i>
			</div>
			<a href="<?php echo site_url('main/order') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div><!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-green">
			<div class="inner">
				<h3>
					<?php 
						if ($status!='member') {
							echo $cDone; 
						}else{
							echo $countmSuccess;
						}
					?>
				</h3>
				<p>Success Orders</p>
			</div>
			<div class="icon">
				<i class="ion ion-checkmark-circled"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div><!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-yellow">
			<div class="inner">
				<h3>
					<?php 
						if ($status!='member') {
							echo $cTask; 
						}else{
							echo $countmTask;
						}
					?>
				</h3>
				<p>On Task Orders</p>
			</div>
			<div class="icon">
				<i class="ion ion-waterdrop"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div><!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner">
				<h3>
					<?php 
						if ($status!='member') {
							echo $cCancel; 
						}else{
							echo $countmCancel;
						}
					?>
				</h3>
				<p>Canceled Orders</p>
			</div>
			<div class="icon">
				<i class="ion ion-close"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div><!-- ./col -->
</div>