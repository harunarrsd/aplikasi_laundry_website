<div class="aboutus" id="about" style="background:#fff;padding-top:0px;padding:50px 0;">
		<div class="container">
			<center class="wow fadeInDown">
				<h1 style="color:black;">Profile Laundry YO! ?</h1>
				<label class="" style="border-top:solid 3px #999;width:70px;"></label>
				<p>Laundry YO! adalah aplikasi jasa pemesanan Laundry yang mempunyai banyak kelebihan</p>				
			</center>
			<div class="col-md-12">
				<center>
					<div class="row">
						<div class="col-md-4 wow fadeInLeft">
							<div class="panel panel-default mypan">
								<div class="panel-body">
									<i style="font-size:2em;" class=""></i>
									<h2>Cepat</h2>
									<p><label>Laundry YO!</label> menjamin kecepatan dan ketepatan sistem kami untuk pemesanan Anda.</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 wow fadeInUp">
							<div class="panel panel-default mypan">
								<div class="panel-body">
									<i style="font-size:2em;" class=""></i>
									<h2>Mudah</h2>
									<p>Dengan <label>Laundry YO!</label> anda menjadi lebih mudah dalam mengorder Laundry Cucian.</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 wow fadeInRight">
							<div class="panel panel-default mypan">
								<div class="panel-body">
									<i style="font-size:2em;" class=""></i>
									<h2>Aman</h2>
									<p><label>Laundry YO!</label> menjamin keamanan segala data informasi yang Anda masukan di <label>Laundry YO!</label>.</p>
								</div>
							</div>
						</div>
					</div>
				</center>
			</div>

		</div>
	</div>
	<div class="mFoot">
		<div class="col-md-12">
			<div class="container">
				<center>
					<div class="sosmed">
						<a target="_blank" href="https://facebook.com/fahmiprasanda" class="fac"><i class="fa-facebook fa"></i></a>
						<a target="_blank" href="https://twitter.com/@fahmi_gcoe" class="twit"><i class="fa-twitter fa"></i></a>
						<a target="_blank" href="https://plus.google.com/+fahmiprasanda" class="gplus"><i class="fa-google-plus fa"></i></a>
					</div>
					<label style="border-top:solid 1px #999;width:15%;margin-top:18px;"></label>
					<p style="margin:0;">Copyright &copy; Harun & Dimazh Setiawan- Rekayasa Perangkat Lunak - XII RPL 2<br> All Right Reserved</p>
				</center>
			</div>
		</div>
	</div>
<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>dist/js/owl.carousel.js"></script> -->
<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/wow.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>dist/js/smooth-scroll.js"></script>
<script src="<?php echo base_url(); ?>dist/preloader/js/classie.js"></script>
<script src="<?php echo base_url(); ?>dist/preloader/js/pathLoader.js"></script>
<script src="<?php echo base_url(); ?>dist/preloader/js/main.js"></script>
<script  type="text/javascript">
	$(function() {
		$('a.rf[href*=#]:not([href=#])').each(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

				var targetId = $(this.hash), targetAnchor = $('[name=' + this.hash.slice(1) +']');
				var target = targetId.length ? targetId : targetAnchor.length ? targetAnchor : false;
				if (target) {
					var targetOffset = target.offset().top;
					$(this).click(function() {
						$(".navbar-nav li a").removeClass("active");
						$(this).addClass('active');
						$('html, body').animate({scrollTop: targetOffset}, 1000);
						return false;
					});
				}
			}
		});
	});
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})
	wow = new WOW(
	{
		animateClass: 'animated',
		offset:       100,
	}
	);
	wow.init();
	$(document).ready(function() {
		var menu = $('.navmenu'),
		pos = menu.offset(),
		ab = $("#about"),
		abpos = ab.offset(),
		sv = $("#services"),
		svpos = sv.offset(),
		team = $("#teams"),
		teampos = team.offset(),
		port = $("#portfolio"),
		portpos = port.offset(),
		testi = $("#testimonials"),
		testipos = testi.offset();
		$(window).scroll(function(){
			if($(this).scrollTop() >= pos.top+menu.height() && menu.hasClass('normal')){
				menu.fadeOut('fast', function(){
					$(this).removeClass('normal navmenu').addClass('navmenunew').fadeIn('fast');
					// $("#logo").hide();
				});
			} else if($(this).scrollTop() <= pos.top && menu.hasClass('navmenunew')){
				menu.fadeOut('fast', function(){
					$(this).removeClass('navmenunew').addClass('normal navmenu').fadeIn('fast');
					// $("#logo").fadeIn();
				});
			};
			if ($(this).scrollTop() >= abpos.top) {
				$("#aboutref").addClass('active');
				$("#svref").removeClass('active');
				$("#portref").removeClass('active');
				$("#contref").removeClass('active');
				$("#tref").removeClass('active');
			}else{
				$("#aboutref").removeClass('active');
			};
			if ($(this).scrollTop() >= svpos.top) {
				$("#aboutref").removeClass('active');
				$("#tref").removeClass('active');
				$("#portref").removeClass('active');
				$("#contref").removeClass('active');
				$("#svref").addClass('active');
			}else{
				$("#svref").removeClass('active');
			};
			if ($(this).scrollTop() >= teampos.top) {
				$("#aboutref").removeClass('active');
				$("#svref").removeClass('active');
				$("#portref").removeClass('active');
				$("#contref").removeClass('active');
				$("#tref").addClass('active');
			}else{
				$("#tref").removeClass('active');
			};
			if ($(this).scrollTop() >= portpos.top) {
				$("#aboutref").removeClass('active');
				$("#tref").removeClass('active');
				$("#svref").removeClass('active');
				$("#contref").removeClass('active');
				$("#portref").addClass('active');
			}else{
				$("#portref").removeClass('active');
			};
			if ($(this).scrollTop() >= contpos.top) {
				$("#aboutref").removeClass('active');
				$("#svref").removeClass('active');
				$("#portref").removeClass('active');
				$("#tref").removeClass('active');
				$("#contref").addClass('active');
			}else{
				$("#contref").removeClass('active');
			};
			// console.log($(this).scrollTop());
		});

	});
	
</script>
</body>
</html>
