<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>Version</b> 0.0.1
	</div>
	<strong>Copyright © 2016 <a href="http://sofensys.com">Harun & Dimazh Setiawan- Rekayasa Perangkat Lunak - XII RPL 2</a>.</strong> All rights reserved.
</footer>
</div>
<script src="<?php echo base_url(); ?>lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
	// $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="<?php echo base_url(); ?>dist/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>lte/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url(); ?>lte/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url(); ?>lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo base_url(); ?>lte/plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?php echo base_url(); ?>lte/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>lte/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?php echo base_url(); ?>lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>lte/plugins/fastclick/fastclick.min.js"></script>
<script src="<?php echo base_url(); ?>lte/dist/js/app.min.js"></script>
<script src="<?php echo base_url(); ?>lte/dist/js/demo.js"></script>
<?php
	if ($status=='member' AND $this->uri->segment(2)=='new_order') {
?>
<script src="<?php echo base_url(); ?>dist/js/neworderScript.js"></script>
<?php } ?>
</body>
</html>