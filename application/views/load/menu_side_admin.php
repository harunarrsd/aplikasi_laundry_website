<aside class="main-sidebar skin-red">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo base_url(); ?>lte/dist/img/avatar.png" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p><?php echo $nama; ?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- search form -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<li class="<?php if($this->uri->segment(2)=="" OR $this->uri->segment(2)=="admin"){echo "active";}else{echo "";} ?> treeview">
				<a href="<?php echo site_url(); ?>">
					<i class="fa fa-dashboard"></i> <span>Dashboard</span>
				</a>
			</li>
			
			<?php if ($status=='admin') {?>
			<li class="treeview <?php if($this->uri->segment(2)=="members"){echo "active";}else{echo "";} ?>">
				<a href="<?php echo site_url('main/member'); ?>">
					<i class="fa fa-user"></i>
					<span>Daftar Member</span>
				</a>
			</li>
			<li class="treeview <?php if($this->uri->segment(2)=="staff"){echo "active";}else{echo "";} ?>">
				<a href="<?php echo site_url('main/staff'); ?>">
					<i class="fa fa-group"></i>
					<span>Daftar Karyawan</span>
				</a>
			</li>
			<?php }else if ($status=='member') {?>
			<li class="treeview <?php if($this->uri->segment(2)=="new_order"){echo "active";}else{echo "";} ?>">
				<a href="<?php echo site_url('main/new_order'); ?>">
					<i class="fa fa-edit"></i>
					<span>Laundry</span>
				</a>
			</li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-cog"></i>
					<span>Menu</span>
					<i class="fa fa-angle-left pull-right"></i>
					<!-- <span class="label label-primary pull-right">4</span> -->
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo site_url('main/profile'); ?>"><i class="fa fa-circle-o"></i> Akun Saya</a></li>
					<li><a href="#"><i class="fa fa-circle-o"></i> Bantuan</a></li>
					<li><a href="#"><i class="fa fa-circle-o"></i> Hubungi Kami</a></li>
				</ul>
			</li>
			<li class="header">Pesanan Saya</li>
			<li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>On Order</span></a></li>
			<li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>On Task</span></a></li>
			<li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Done</span></a></li>
			<?php } ?>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>