<header class="main-header">
	<!-- Logo -->
	<a href="#" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>L</b>Y</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>Laundry</b>YO!</span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- Messages: style can be found in dropdown.less-->

					
				<!-- Notifications: style can be found in dropdown.less -->
				
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo base_url(); ?>lte/dist/img/avatar.png" class="user-image" alt="User Image">
						<span class="hidden-xs"><?php echo ucfirst($nama); ?></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
							<img src="<?php echo base_url(); ?>lte/dist/img/avatar.png" class="img-circle" alt="User Image">
							<p>
								<?php echo ucfirst($nama); ?>
								<small>Member since FEB. 2016</small>
							</p>
						</li>
						<!-- Menu Body -->
						<!-- <li class="user-body">
							<div class="col-xs-4 text-center">
								<a href="#">Followers</a>
							</div>
							<div class="col-xs-4 text-center">
								<a href="#">Sales</a>
							</div>
							<div class="col-xs-4 text-center">
								<a href="#">Friends</a>
							</div>
						</li> -->
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat">Profile</a>
							</div>
							<div class="pull-right">
								<a href="<?php echo site_url('processes/log_out'); ?>" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul>
				</li>
				<!-- Control Sidebar Toggle Button -->
				<!-- <li>
					<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
				</li> -->
			</ul>
		</div>
	</nav>
</header>