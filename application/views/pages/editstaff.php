<!-- Header of a page -->
<?php $this->load->view('load/head_admin'); ?>
<!-- Header of a page -->


    <?php $this->load->view('load/menu_top_admin'); ?>

    <?php
        $this->load->view('load/menu_side_admin');
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            
            <?php $this->load->view('load/breadcumb'); ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="box">
                <div class="box-header with-border">
                <h3 class="box-title">Daftar Pesanan Saya</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-bordered">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>NIK</th>
                            <th>Nama Karyawan</th>
                            <th>Alamat Karyawan</th>
                            <th>Telp Karyawan</th>
                            <th>Jenis Kelamin</th>
                            <th>Status Karyawan</th>
                            <th>Action</th>
                        </tr>
                        <?php $no=1;
                            foreach ($getkaryawan->result_array() as $karyawan) {
                         ?>
                        <tr>
                            <td><?php echo $no++;?></td>
                            <form method="post" action="<?php echo site_url('main/acteditstaff'); ?>">
                            <input type="hidden" value="<?php echo $karyawan['id_karyawan']; ?>" id="id_karyawan" name="id_karyawan">
                            <td><input type="number" value="<?php echo $karyawan['nik']; ?>" id="nik" name="nik"></td>
                            <td><input type="text" value="<?php echo $karyawan['nama_karyawan']; ?>" name="nama_karyawan" id="nama_karyawan"></td>
                            <td><input type="text" value="<?php echo $karyawan['alamat_karyawan']; ?>" id="alamat_karyawan" name="alamat_karyawan"></td>
                            <td><input type="number" value="<?php echo $karyawan['telp_karyawan']; ?>" id="telp_karyawan" name="telp_karyawan"></td>
                            <td><select name="jk_karyawan" id="jk_karyawan">
                                <option value="laki-laki">laki-laki</option>
                                <option value="perempuan">perempuan</option>
                            </select>
                            <td><select name="status_karyawan" id="status_karyawan">
                                <option value="aktif" name="aktif" id="aktif">aktif</option>
                                <option value="tidak_aktif" name="tidak_aktif" id="tidak_aktif">pasif</option>
                            </select></td>
                            <td>
                                <input type="submit" value="edit"></input>
                            </td>
                        </tr>
                        </form>
                        <?php } ?>
                    </tbody></table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                </div>
            </div>
        </section>
    </div>
    

<!-- Footer of a page-->
<?php $this->load->view('load/foot_admin'); ?>
<!-- Footer of a page-->