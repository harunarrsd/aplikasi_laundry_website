<!-- Header of a page -->
<?php $this->load->view('load/head_admin'); ?>
<!-- Header of a page -->


	<?php $this->load->view('load/menu_top_admin'); ?>

	<?php
		$this->load->view('load/menu_side_admin');
	?>

	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			
			<?php $this->load->view('load/breadcumb'); ?>
		</section>

		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<?php $this->load->view('load/dashboard'); ?>
		</section>
	</div>
	

<!-- Footer of a page-->
<?php $this->load->view('load/foot_admin'); ?>
<!-- Footer of a page-->