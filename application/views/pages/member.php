<!-- Header of a page -->
<?php $this->load->view('load/head_admin'); ?>
<!-- Header of a page -->


    <?php $this->load->view('load/menu_top_admin'); ?>

    <?php
        $this->load->view('load/menu_side_admin');
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            
            <?php $this->load->view('load/breadcumb'); ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="box">
                <div class="box-header with-border">
                <h3 class="box-title">Daftar Pesanan Saya</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-bordered">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>Username</th>
                            <th>create date</th>
                            <th>status</th>
                        </tr>
                        <?php $no=1;
                            foreach ($getmember->result_array() as $member) {
                         ?>
                        <tr>
                            <td><?php echo $no++;?></td>
                            <td><?php echo $member['username'] ?></td>
                            <td><?php echo $member['created_date']; ?></td>
                            <td><?php echo $member['status']; ?></td>
                            <td>
                                
                                <a style="color:#fff;" data-toggle="tooltip" data-title="EDIT"><span class="badge bg-green"><i class="glyphicon-pencil glyphicon"></i></span></a>
                                
                                <a style="color:#fff;" data-toggle="tooltip" data-title="DELETE"><span class="badge bg-red"><i class="glyphicon-remove glyphicon"></i></span></a>
                                
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody></table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                </div>
            </div>
        </section>
    </div>
    

<!-- Footer of a page-->
<?php $this->load->view('load/foot_admin'); ?>
<!-- Footer of a page-->