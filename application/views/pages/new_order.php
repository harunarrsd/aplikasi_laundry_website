<!-- Header of a page -->
<?php $this->load->view('load/head_admin'); ?>
<!-- Header of a page -->

<?php $this->load->view('load/menu_top_admin'); ?>

<?php
$this->load->view('load/menu_side_admin');
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php $this->load->view('load/breadcumb'); ?>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="alert alert-success hide" id="alFormOrder">
			
		</div>
		<div class="box box-primary" style="">
			<div class="box-header with-border">
				<h3 class="box-title">Form Pemesanan Baru</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" action="javascript:void();" id="frmOrder">
				<div class="loader box-body" style="position: absolute;width: 100%;background: rgba(255,255,255,.9);padding:13.3%;z-index: 1;">
					<center><img src="<?php echo base_url('dist/img/loading.gif'); ?>" alt=""></center>
				</div>
				<div class="box-body" id="formOrdernya">
					<div class="form-group form-group-no" id="eJenisB">
						<label for="jenis_b">Jenis Barang</label>
						<select class="form-control select2 select2-hidden-accessible" id="jenis_b" name="jenis_barang">
							<option value="-1">Pilih Jenis Barang</option>
							<option value="satuan">Satuan</option>
							<option value="kiloan">Kiloan</option>
						</select>
					</div>
					<div id="kilo">
						<!-- <div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Tanggal Barang Diambil</label>
									<input name="tgl_diambil" class="form-control" type="date">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Tanggal Barang Selesai</label>
									<input name="tgl_selesai" class="form-control" type="date">
								</div>
							</div>
						</div> -->
						
						
						<div class="form-group form-group-no" id="eJenisC">
							<label for="jenis_c">Jenis Cuci</label>
							<select class="form-control select2 select2-hidden-accessible" id="jenis_c" name="jenis_cuci">
								<option value="-1">Pilih Jenis Cuci</option>
								<option value="1">Cuci Basah</option>
								<option value="2">Cuci Kering</option>
								<option value="3">Cuci Setrika</option>
							</select>
						</div>
						<div class="form-group form-group-no" id="eJenisP">
							<label for="jenis_c">Jenis Paket</label>
							<select class="form-control select2 select2-hidden-accessible" id="jenis_p" name="jenis_paket">
								<option value="-1">Pilih Jenis Paket</option>
								<option value="reguler">Reguler | 1-2 Hari</option>
								<option value="express">Express | 3-5 Hari</option>
							</select>
						</div>
						<div class="form-group form-group-no" id="eBerat">
							<label>Berat Barang</label>
							<input type="number" class="form-control" id="berat" name="berat" placeholder="Masukan berat barang cucian">
							<p id="alBerat" class="help-block"></p>
						</div>
						<h4 style="margin:2% 0 0 0;">Perkiraan Total</h4>
						<hr style="margin:10px 0 10px 0;">
						<div class="form-group">
							<label>Harga per kilo</label>
							<input type="text" class="form-control" readonly="" disabled="" value="" id="hpk_view">
							<input type="hidden" id="hpk" readonly="" name="hpk">
						</div>
						<div class="form-group">
							<label>Harga Total</label>
							<input type="text" class="form-control" readonly="" disabled="" value="" id="total_view">
							<input type="hidden" name="total" readonly="" id="total">
						</div>
					<p class="help-block">*) Untuk paket Express diperlukan biaya tambahan senilai Rp.5000</p>
					</div>
				</div><!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Kirim</button>
				</div>
			</form>
		</div>
	</section>
</div>

<!-- Footer of a page-->
<?php $this->load->view('load/foot_admin'); ?>
<!-- Footer of a page-->