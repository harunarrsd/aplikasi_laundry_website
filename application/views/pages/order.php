<!-- Header of a page -->
<?php $this->load->view('load/head_admin'); ?>
<!-- Header of a page -->


    <?php $this->load->view('load/menu_top_admin'); ?>

    <?php
        $this->load->view('load/menu_side_admin');
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            
            <?php $this->load->view('load/breadcumb'); ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Filter Data</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                    <div class="box-body">
                        <form role="form" id="formFilter">
                        <div class="row">
                            <div class="col-xs-10">
                                <select class="form-control" name="jnisC" id="jnisC">
                                    <option value="all" selected="">Semua Jenis Cuci</option>
                                    <?php foreach ($jnis as $jc) {?>
                                    <option value="<?php echo $jc->id_jenis; ?>"><?php echo $jc->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <button class="btn btn-block btn-flat btn-primary">Filter</button>
                            </div>
                        </div>
                        </form>
                    </div>
            </div>
            <div class="box-default box">
                <div class="box-header with-border">
                    <h3 class="box-title">List Data</h3>
                </div>
                <div class="box-body no-padding table-responsive" id="tableAllOrder">
                    
                </div>
                <div class="box-body no-padding table-responsive" id="tableAll">
                    <table class="table" id="tbl">
                        <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Nama Pesanan</th>
                                <th>Konsumen</th>
                                <th>Jenis Cuci</th>
                                <th>Status</th>
                                <th width="100">Aksi</th>
                            </tr>
                            <?php $no=1;
                            foreach ($allOrdersData as $on) {
                                ?>
                                <tr>
                                    <td><?php echo $no++; ?>.</td>
                                    <td><?php echo "Pesanan Laundry ".$on->jumlah."kg" ?></td>
                                    <td><?php echo $on->nama_konsumen; ?></td>
                                    <td><?php echo $on->nama; ?></td>
                                    <td><?php echo $on->status; ?></td>
                                    <td>
                                        <a href="<?php echo site_url('main/verifyorder/'.$on->id_transaksi); ?>"><button class="btn btn-default"><i class="glyphicon-pencil glyphicon"></i></button></a>
                                        <button class="btn btn-danger"><i class="glyphicon-trash glyphicon"></i></button>
                                    </td>
                                </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
    

<!-- Footer of a page-->
<?php $this->load->view('load/foot_admin'); ?>
<!-- Footer of a page-->