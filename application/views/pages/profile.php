<!-- Header of a page -->
<?php $this->load->view('load/head_admin'); ?>
<!-- Header of a page -->

<?php $this->load->view('load/menu_top_admin'); ?>

<?php
$this->load->view('load/menu_side_admin');
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<?php $this->load->view('load/breadcumb'); ?>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-3">

				<!-- Profile Image -->
				<div class="box box-primary">
					<div class="box-body box-profile">
					<img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>lte/dist/img/avatar.png" alt="User profile picture">
						<h3 class="profile-username text-center"><?php echo $nama; ?></h3>
						<p class="text-muted text-center">Member Sejak <?php echo $created_date; ?></p>

					</div><!-- /.box-body -->
				</div><!-- /.box -->

				<!-- About Me Box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">About Me</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<strong><i class="fa fa-home margin-r-5"></i>  Alamat</strong>
						<p class="text-muted">
							Permata Duta E3/06 Sukmajaya, Sukmajaya DEPOK
						</p>

						<hr>

						<strong><i class="fa fa-send margin-r-5"></i> Email</strong>
						<p class="text-muted">Fahmiprasanda@computerscience.id</p>

						<hr>

						<strong><i class="fa fa-phone margin-r-5"></i> Nomor Telepon</strong>
						<p>081317906140</p>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>

			<div class="col-md-9">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#timeline" data-toggle="tab" aria-expanded="true">Aktifitas</a></li>
						<li class=""><a href="#settings" data-toggle="tab" aria-expanded="false">Pengaturan</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="timeline">
							<!-- The timeline -->
							<ul class="timeline timeline-inverse">
								<?php 
									foreach ($aktifitas as $a) {
								?>
								<!-- timeline time label -->
								<!-- <li class="time-label">
									<span class="bg-red">
										10 Feb. 2014
									</span>
								</li> -->
								<!-- /.timeline-label -->
								<!-- timeline item -->
								<!-- <li>
									<i class="fa fa-envelope bg-blue"></i>
									<div class="timeline-item">
										<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
										<h3 class="timeline-header"><a href="#">Admin</a> sent you a notification</h3>
										<div class="timeline-body">
											Hallo, saya dari Admin website LaundryKu sudah memeriksa orderan Anda...
										</div>
										<div class="timeline-footer">
											<a class="btn btn-primary btn-xs">Read more</a>
										</div>
									</div>
								</li> -->
								<!-- END timeline item -->
								<li class="time-label">
									<span class="bg-red">
										<?php echo $a->tgl_history; ?>
									</span>
								</li>
								<!-- timeline item -->
								<li>
									<i class="fa fa-user-add bg-aqua"></i>
									<div class="timeline-item">
										<!-- <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span> -->
										<h3 class="timeline-header no-border"><?php echo $a->isi_history; ?></h3>
									</div>
								</li>
								<?php } ?>
								<!-- END timeline item -->
								<li>
									<i class="fa fa-clock-o bg-gray"></i>
								</li>
							</ul>
						</div><!-- /.tab-pane -->
							<?php 
								
							 ?>
						<div class="tab-pane" id="settings">
							<form class="form-horizontal">
								<div class="form-group">
								<input type="hidden" name="id_user" id="id_user" value="<?php ?>">
									<label for="inputName" class="col-sm-2 control-label">Name</label>
									<div class="col-sm-10">
										<input type="email" class="form-control" id="inputName" placeholder="Name">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail" class="col-sm-2 control-label">Email</label>
									<div class="col-sm-10">
										<input type="email" class="form-control" id="inputEmail" placeholder="Email">
									</div>
								</div>
								<div class="form-group">
									<label for="inputName" class="col-sm-2 control-label">Name</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="inputName" placeholder="Name">
									</div>
								</div>
								<div class="form-group">
									<label for="inputExperience" class="col-sm-2 control-label">Experience</label>
									<div class="col-sm-10">
										<textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label for="inputSkills" class="col-sm-2 control-label">Skills</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="inputSkills" placeholder="Skills">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<div class="checkbox">
											<label>
												<input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
											</label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="submit" class="btn btn-danger">Submit</button>
									</div>
								</div>
							</form>
						</div><!-- /.tab-pane -->
					</div><!-- /.tab-content -->
				</div><!-- /.nav-tabs-custom -->
			</div>

		</div>
	</section>
</div>

<!-- Footer of a page-->
<?php $this->load->view('load/foot_admin'); ?>
<!-- Footer of a page