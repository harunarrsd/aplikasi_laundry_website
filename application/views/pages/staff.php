<!-- Header of a page -->
<?php $this->load->view('load/head_admin'); ?>
<!-- Header of a page -->


    <?php $this->load->view('load/menu_top_admin'); ?>

    <?php
        $this->load->view('load/menu_side_admin');
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            
            <?php $this->load->view('load/breadcumb'); ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="box">
                <div class="box-header with-border">
                <h3 class="box-title">Daftar Pesanan Saya</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table class="table table-bordered">
                        <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>NIK</th>
                            <th>Nama Karyawan</th>
                            <th>Alamat Karyawan</th>
                            <th>Telp Karyawan</th>
                            <th>Jenis Kelamin</th>
                            <th>Status Karyawan</th>
                            <th>Action</th>
                        </tr>
                        <?php $no=1;
                            foreach ($getkaryawan->result_array() as $karyawan) {
                         ?>
                        <tr>
                            <td><?php echo $no++;?></td>
                            <td><?php echo $karyawan['nik']; ?></td>
                            <td><?php echo $karyawan['nama_karyawan']; ?></td>
                            <td><?php echo $karyawan['telp_karyawan']; ?></td>
                            <td><?php echo $karyawan['telp_karyawan']; ?></td>
                            <td><?php echo $karyawan['jk_karyawan']; ?></td>
                            <td><?php echo $karyawan['status_karyawan']; ?></td>
                            <td>
                                
                                <a style="color:#fff;" href="<?php echo site_url('main/editstaff') ?>" data-toggle="tooltip" data-title="EDIT"><span class="badge bg-green"><i class="glyphicon-pencil glyphicon"></i></span></a>
                                
                                <a style="color:#fff;" data-toggle="tooltip" data-title="DELETE"><span class="badge bg-red"><i class="glyphicon-remove glyphicon"></i></span></a>
                                
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody></table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                </div>
            </div>
        </section>
    </div>
    

<!-- Footer of a page-->
<?php $this->load->view('load/foot_admin'); ?>
<!-- Footer of a page-->