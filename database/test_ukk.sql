-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15 Feb 2016 pada 02.17
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_ukk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `nama_barang` varchar(250) NOT NULL,
  `stok` int(11) NOT NULL,
  `date_update` date NOT NULL,
  `status` enum('aktif','tidak_aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `history`
--

CREATE TABLE `history` (
  `id_history` int(11) NOT NULL,
  `id_konsumen` int(11) NOT NULL,
  `judul_history` varchar(255) NOT NULL,
  `isi_history` text NOT NULL,
  `tgl_history` datetime NOT NULL,
  `status` enum('aktif','tidak_aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `history`
--

INSERT INTO `history` (`id_history`, `id_konsumen`, `judul_history`, `isi_history`, `tgl_history`, `status`) VALUES
(4, 7, 'user_created', 'Kamu membuat akun di LaundryKu', '2016-02-09 02:09:13', 'aktif'),
(5, 8, 'User Created', 'Kamu membuat akun di LaundryKu', '2016-02-15 01:08:07', 'aktif'),
(6, 8, 'new_order', 'Kamu telah memesan Pesanan Baru dengan detail berat barang : 7Kg, Jenis Cuci : 2, Jenis Paket : express, dan Jenis Barang : kiloan', '2016-02-15 01:23:07', 'aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nik` int(11) NOT NULL,
  `nama_karyawan` varchar(110) NOT NULL,
  `alamat_karyawan` text NOT NULL,
  `telp_karyawan` bigint(20) NOT NULL,
  `jk_karyawan` enum('laki-laki','perempuan') NOT NULL,
  `status_karyawan` enum('aktif','tidak_aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `id_user`, `nik`, `nama_karyawan`, `alamat_karyawan`, `telp_karyawan`, `jk_karyawan`, `status_karyawan`) VALUES
(1, 2, 1313, 'Karyawan Satu', 'Ada lah pokonya dimana wae', 8994217891, 'laki-laki', 'aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konsumen`
--

CREATE TABLE `konsumen` (
  `id_konsumen` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_konsumen` varchar(100) NOT NULL,
  `email_konsumen` varchar(255) NOT NULL,
  `alamat_konsumen` text NOT NULL,
  `telp_konsumen` bigint(20) NOT NULL,
  `kode_konsumen` varchar(20) NOT NULL,
  `avatar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konsumen`
--

INSERT INTO `konsumen` (`id_konsumen`, `id_user`, `nama_konsumen`, `email_konsumen`, `alamat_konsumen`, `telp_konsumen`, `kode_konsumen`, `avatar`) VALUES
(5, 7, 'Lagi Lagi', '', '', 0, '', ''),
(6, 8, 'Sony Surahman', '', '', 0, '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian`
--

CREATE TABLE `pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `tgl_pembelian` datetime NOT NULL,
  `total_biaya` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `p_detail`
--

CREATE TABLE `p_detail` (
  `id_detail` int(11) NOT NULL,
  `id_pembelian` int(11) NOT NULL,
  `jumlah_barang` int(11) NOT NULL,
  `status_barang` enum('aktif','tidak_aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supp` varchar(255) NOT NULL,
  `alamat_supp` text NOT NULL,
  `telp_supp` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tarif`
--

CREATE TABLE `tarif` (
  `id_tarif` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `diskon_price` int(11) NOT NULL,
  `tarif_asli` int(11) NOT NULL,
  `tarif` int(11) NOT NULL,
  `tarif_after_diskon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tarif`
--

INSERT INTO `tarif` (`id_tarif`, `id_transaksi`, `diskon_price`, `tarif_asli`, `tarif`, `tarif_after_diskon`) VALUES
(1, 1, 0, 28000, 33000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_konsumen` int(11) NOT NULL,
  `coupon` bigint(20) NOT NULL,
  `tgl_transaksi` datetime NOT NULL,
  `tgl_ambil` datetime NOT NULL,
  `status` enum('on_order','on_task','done','cancel') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_konsumen`, `coupon`, `tgl_transaksi`, `tgl_ambil`, `status`) VALUES
(1, 8, 0, '2016-02-15 01:42:33', '0000-00-00 00:00:00', 'on_order');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_detail`
--

CREATE TABLE `transaksi_detail` (
  `id_detail` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `jenis_barang` enum('satuan','kiloan') NOT NULL,
  `jenis_cuci` enum('cuci_basah','cuci_kering','cuci_setrika') NOT NULL,
  `jenis_paket` enum('reguler','express') NOT NULL,
  `diskon` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL COMMENT 'jumlah kilo atau satuan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_detail`
--

INSERT INTO `transaksi_detail` (`id_detail`, `id_transaksi`, `jenis_barang`, `jenis_cuci`, `jenis_paket`, `diskon`, `jumlah`) VALUES
(1, 1, 'kiloan', 'cuci_kering', 'express', 0, 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_jenis_cuci_kilo`
--

CREATE TABLE `t_jenis_cuci_kilo` (
  `id_jenis` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `harga` int(11) NOT NULL COMMENT 'per 1kg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_jenis_cuci_kilo`
--

INSERT INTO `t_jenis_cuci_kilo` (`id_jenis`, `nama`, `harga`) VALUES
(1, 'Cuci Basah', 2000),
(2, 'Cuci Kering', 4000),
(3, 'Cuci Setrika', 6000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(110) NOT NULL,
  `password` varchar(150) NOT NULL,
  `type_user` enum('member','karyawan','admin') NOT NULL,
  `created_date` date NOT NULL,
  `status` enum('aktif','tidak_aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `type_user`, `created_date`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '2016-02-09', 'aktif'),
(2, 'karyawan', '9e014682c94e0f2cc834bf7348bda428', 'karyawan', '2016-02-10', 'aktif'),
(7, 'lagi lagi', 'e691fe545314b7b56c5d82702f2be359', 'member', '2016-02-15', 'aktif'),
(8, 'son', '498d3c6bfa033f6dc1be4fcc3c370aa7', 'member', '2016-02-15', 'aktif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id_history`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `konsumen`
--
ALTER TABLE `konsumen`
  ADD PRIMARY KEY (`id_konsumen`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `p_detail`
--
ALTER TABLE `p_detail`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `tarif`
--
ALTER TABLE `tarif`
  ADD PRIMARY KEY (`id_tarif`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `t_jenis_cuci_kilo`
--
ALTER TABLE `t_jenis_cuci_kilo`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `konsumen`
--
ALTER TABLE `konsumen`
  MODIFY `id_konsumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `p_detail`
--
ALTER TABLE `p_detail`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tarif`
--
ALTER TABLE `tarif`
  MODIFY `id_tarif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_jenis_cuci_kilo`
--
ALTER TABLE `t_jenis_cuci_kilo`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
